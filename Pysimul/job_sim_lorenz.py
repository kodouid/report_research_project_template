"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_lorenz.py
```
"""

import matplotlib.pyplot as plt
from fluidsim.solvers.models0d.lorenz.solver import Simul
import numpy as np
import sys
from pathlib import Path
params = Simul.create_default_params()

here = Path(__file__).absolute().parent
path_dir_save = here / "../fig/"
path_dir_save.mkdir(exist_ok=True)

params.time_stepping.deltat0 = 0.03
params.time_stepping.t_end = 30

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XYZ()
# see also the other plot_* methods of sim.output.print_stdout
ax = plt.gca()
fig = ax.figure
plt.savefig(path_dir_save / "lorenz_fig1.png")

plt.show()
